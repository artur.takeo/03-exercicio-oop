package com.itau.dados;

public class Impressora {

	public static void imprimir(Resultado resultado) {
		System.out.println(resultado);
	}
	
	public static void imprimir(Resultado[] resultados) {
		for(Resultado resultado: resultados) {
			System.out.println(resultado);
		}
	}
}
